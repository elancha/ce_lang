# CE
The is the programming language that I created because I felt like. The idea is to learn [Z3](https://github.com/Z3Prover/z3) (or some other SMT theorem prover) and make my first language.

We will see how it goes. Also the idea is for this language to be inherently literate (see [Literate Programming](literateprogramming.org)) and we will have to develop a debugger and an IDE for it. That for sure has to be fun.
